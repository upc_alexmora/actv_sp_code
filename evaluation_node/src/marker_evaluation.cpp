

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ros/ros.h>
#include <std_msgs/Float32.h>

#include "geometry_msgs/Quaternion.h"
#include "geometry_msgs/Vector3.h"
#include "sensor_msgs/Image.h"

#include <tf/tf.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_listener.h>
#include <tf/tfMessage.h>
#include "tf2_msgs/TFMessage.h"

using namespace std;

// ----------------------------------------------------------------------
// Global variables definition

// a) For min-max distance measurements:
geometry_msgs::Vector3 xyzPosition;
float dist;
float maxDist = 0;
float minDist = 999999;

// b) For min-max tilt angles:
// geometry_msgs::Quaternion quatOrientation;
double roll;
float maxRoll = 0;
float minRoll = 999999;
double pitch;
float maxPitch = 0;
float minPitch = 999999;
double yaw;
float maxYaw = 0;
float minYaw = 999999;

// c) For detection rates:
int frameCounter = 0;
int tfCounter = 0;




// ----------------------------------------------------------------------

// ----------------------------------------------------------------------
// Callback FUNCTIONS

// Received marker transform.
void transformCallback(const tf2_msgs::TFMessage msg)
{
  // Save the pose of the marker received. Compute distance to camera
  // and rotation with respect to it (in roll-pitch-yaw angles).
  // ROS_INFO("TF Callback");

  // for (int k = 0; k < msg.transforms.size(); k++)
  // {
    // Obtain position and orientation of the marker.
    xyzPosition = msg.transforms[0].transform.translation;
    // quatOrientation = msg.transforms[0].transform.rotation;
    tf::Quaternion quatOrientation(msg.transforms[0].transform.rotation.x,
      msg.transforms[0].transform.rotation.y,
      msg.transforms[0].transform.rotation.z,
      msg.transforms[0].transform.rotation.w);
    // Compute euclidean distance and rotation in Euler angles.
    dist = sqrt( pow(xyzPosition.x, 2) + pow(xyzPosition.y, 2) + pow(xyzPosition.z, 2) );
    tf::Matrix3x3 quat2rpy(quatOrientation);
    quat2rpy.getRPY(roll, pitch, yaw);

    // Transform angles from radians to degrees for easier readability.
    roll = roll * 180/M_PI;
    pitch = pitch * 180/M_PI;
    yaw = yaw * 180/M_PI;

    // Update min-max values.
    // ----------------------
    if (dist < minDist)
    {
      minDist = dist;
    }

    if (dist > maxDist)
    {
      maxDist = dist;
    }

    if (roll < minRoll)
    {
      minRoll = roll;
    }

    if (roll > maxRoll)
    {
      maxRoll = roll;
    }

    if (pitch < minPitch)
    {
      minPitch = pitch;
    }

    if (pitch > maxPitch)
    {
      maxPitch = pitch;
    }

    if (yaw < minYaw)
    {
      minYaw = yaw;
    }

    if (yaw > maxYaw)
    {
      maxYaw = yaw;
    }
    // ----------------------

  // }
  tfCounter += 1;
}

// Received image from camera.
void imageCallback(const sensor_msgs::Image::ConstPtr& msg)
{
  // Save the pose of the marker received. Compute distance to camera
  // and rotation with respect to it (in roll-pitch-yaw angles).
  // ROS_INFO("Image Callback");
  frameCounter += 1;
}

// Timer callback.
void timerCallback(const ros::TimerEvent& msg)
{
  // Show current experiment data.
  ROS_INFO("Last known distance: %f", dist);
  ROS_INFO("Global minimum distance: %f", minDist);
  ROS_INFO("Global maximum distance: %f", maxDist);

  ROS_INFO("Last known angles: Roll=%f, Pitch=%f, Yaw=%f", roll, pitch, yaw);
  ROS_INFO("Global minimum angles: Roll=%f, Pitch=%f, Yaw=%f", minRoll, minPitch, minYaw);
  ROS_INFO("Global maximum angles: Roll=%f, Pitch=%f, Yaw=%f", maxRoll, maxPitch, maxYaw);

  ROS_INFO("Current transforms counter: %i", tfCounter);
  ROS_INFO("Current frames counter: %i", frameCounter);
  ROS_INFO("");
}

// ----------------------------------------------------------------------



int main(int argc, char** argv)
{
  // Initialize ROS
  ros::init(argc, argv, "marker_evaluation");

  // Read the transform topic as an argument to the function
  string tfTopic;
  if (argc > 1)
  {
     tfTopic = argv[1];
  }

  //ROS_INFO("Topic: %s", tfTopic);

  // Subscribers for transforms and image topics
  ros::NodeHandle nh;
  ros::Subscriber tfListener = nh.subscribe(tfTopic, 100, transformCallback);
  ros::Subscriber imageListener = nh.subscribe("/usb_cam/image_raw", 100, imageCallback);
  // 2 seconds timing interruption for showing debug data in the terminal
  ros::Timer timer = nh.createTimer(ros::Duration(2), timerCallback);

  while(ros::ok())
  {

    ros::spinOnce();
  }
}
